//
//  ListViewController.h
//  
//
//  Created by SakethReddy on 18/02/16.
//
//

#import <UIKit/UIKit.h>
#import <PubNub.h>

@interface ListViewController : UIViewController
<UITableViewDelegate,UITableViewDataSource,PNObjectEventListener>
@property (nonatomic) PubNub *client;
@end
