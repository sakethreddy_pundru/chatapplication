//
//  ListViewController.m
//  
//
//  Created by SakethReddy on 18/02/16.
//
//

#import "ListViewController.h"
#import "ChatViewController.h"
#import <AFNetworking.h>

@interface ListViewController ()

@end

@implementation ListViewController{
    
    NSMutableArray     *filtered;
    UITableView *tableView1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    AFHTTPRequestOperationManager *manager  = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer              = [AFJSONResponseSerializer serializer];
    manager.requestSerializer               = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"XMawrbkbVIUbCdkcbSpvTzQkjP1nC7PeNm15dS6S" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:@"IPF0fVMPySrqPTCvBgoWai7wcWQqrf0qJZDXfm7M" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    
    [manager GET:@"https://api.parse.com/1/classes/PayTM"
      parameters:@{}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             filtered = [NSMutableArray new];
             filtered = [responseObject objectForKey:@"results"];
             
             tableView1 = [[UITableView alloc]init];
             tableView1.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height);
             tableView1.delegate = self;
             tableView1.dataSource = self;
             [self.view addSubview:tableView1];
             [tableView1 reloadData];
             
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
       return [filtered count];
    }


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        NSString *value = @"CellIdentifier";
    
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:value];
    
        if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:value];
            }
    NSDictionary *dict = [filtered objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict valueForKey:@"FirstName"];
    return cell;
    
    }


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array = [filtered valueForKey:@"FirstName"];
    
    if (indexPath.row == 0) {
        NSArray *narray = [array objectAtIndex:indexPath.row];
        ChatViewController *cvc = [[ChatViewController alloc]init];
        cvc.array1 = narray;
        cvc.array2 = array;
        [self.navigationController pushViewController:cvc animated:YES];

  
    }
    if (indexPath.row == 1) {
        NSArray *narray = [array objectAtIndex:indexPath.row];
        ChatViewController *cvc = [[ChatViewController alloc]init];
        cvc.array1 = narray;
        cvc.array2 = array;
        [self.navigationController pushViewController:cvc animated:YES];
        
        
    }

    if (indexPath.row == 2) {
        NSArray *narray = [array objectAtIndex:indexPath.row];
        ChatViewController *cvc = [[ChatViewController alloc]init];
        cvc.array1 = narray;
        cvc.array2 = array;
        [self.navigationController pushViewController:cvc animated:YES];
        
        
    }

    if (indexPath.row == 3) {
        NSArray *narray = [array objectAtIndex:indexPath.row];
        ChatViewController *cvc = [[ChatViewController alloc]init];
        cvc.array1 = narray;
        cvc.array2 = array;
        [self.navigationController pushViewController:cvc animated:YES];
        
        
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
