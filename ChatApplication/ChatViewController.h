//
//  ChatViewController.h
//  
//
//  Created by SakethReddy on 18/02/16.
//
//

#import <UIKit/UIKit.h>
#import <PubNub.h>

@interface ChatViewController : UIViewController<PNObjectEventListener>
@property (nonatomic,strong) PubNub *client;
@property (nonatomic,strong) NSArray *array1;
@property (nonatomic,strong) NSArray *array2;
@end
