//
//  ChatViewController.m
//  
//
//  Created by SakethReddy on 18/02/16.


#import "ChatViewController.h"

@interface ChatViewController ()

@end

@implementation ChatViewController{
    
    
    UITextField     *textField;
    UIButton        *button;
    UITextView      *receiveMessage;
    NSString        *string;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    
    textField               = [[UITextField alloc]init];
    textField.frame         = CGRectMake(30, 250,100, 25);
    [textField setBorderStyle:UITextBorderStyleRoundedRect];
    textField.placeholder   = @"Hi";
    textField.textColor     = [UIColor blackColor];
    [self.view addSubview:textField];
    
    button = [[UIButton alloc]init];
    button.frame            =   CGRectMake(160,350,50, 25);
    button.backgroundColor = [UIColor redColor];
    button.layer.cornerRadius = 10;
    [button setTitle:@"Send" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonSend) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    receiveMessage = [[UITextView alloc]init];
    receiveMessage.frame    =   CGRectMake(30,100,350,50);
    [self.view addSubview:receiveMessage];
    
    PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:@"pub-c-df291025-55f3-4dc4-a41e-e0ce274fdae1"subscribeKey:@"sub-c-df6fdf2c-d475-11e5-a9aa-02ee2ddab7fe"];
    self.client = [PubNub clientWithConfiguration:configuration];
    string = [NSString stringWithFormat:@"%@",self.array1];
    NSLog(@"%@",string);
    configuration.uuid = string;
    self.client = [PubNub clientWithConfiguration:configuration];
    [self.client addListener:self];
    [self.client subscribeToChannels:self.array2  withPresence:NO];
}

-(void)buttonSend{
    

    string = [NSString stringWithFormat:@"%@",self.array1];
    [self.client publish: textField.text toChannel: string storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
          }];
    
    textField.text = @" ";
    
}

-(void)receive{
    
    [self.client subscribeToChannels:self.array1 withPresence:YES];
    
}

- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
    
    if (message.data.actualChannel) {
        
        
        
    }
    else {
        
        
    }
    
    receiveMessage.text = [NSString stringWithFormat:@"%@ \n %@",receiveMessage.text, message.data.message];
    
    NSLog(@"Received message: %@ on channel %@ at %@", message.data.message,
          message.data.subscribedChannel, message.data.timetoken);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
