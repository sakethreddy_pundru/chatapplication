//
//  AppDelegate.h
//  ChatApplication
//
//  Created by SakethReddy on 17/02/16.
//  Copyright (c) 2016 Trusted Software Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

